import * as path from "path";
import { Stack, StackProps, CfnParameter, Fn } from "aws-cdk-lib";
import { Construct } from "constructs";
import * as ec2 from "aws-cdk-lib/aws-ec2";
import * as ecs from "aws-cdk-lib/aws-ecs";
import * as ecr from "aws-cdk-lib/aws-ecr";
import * as iam from "aws-cdk-lib/aws-iam";
import * as ecrAssets from "aws-cdk-lib/aws-ecr-assets";

type GitLabRunnerOnFargateProps = StackProps & {};
export class GitLabRunnerOnFargate extends Stack {
  constructor(scope: Construct, id: string, props: GitLabRunnerOnFargateProps) {
    super(scope, id, props);
    const gitlabURL = new CfnParameter(this, "GitLabURL", {
      default: "https://gitlab.com",
    }).valueAsString;
    const gitlabRunnerToken = new CfnParameter(this, "GitLabRunnerToken", {
      noEcho: true,
    }).valueAsString;

    const vpc = new ec2.Vpc(this, "Vpc", { maxAzs: 1, natGateways: 0 });
    const cluster = new ecs.Cluster(this, "Cluster", {
      vpc,
      // enableFargateCapacityProviders: true, // XXX not supported defaultCapacityProvider
    });
    new ecs.CfnClusterCapacityProviderAssociations(
      this,
      "ClusterCapacityProviderAssociations",
      {
        cluster: cluster.clusterName,
        capacityProviders: ["FARGATE", "FARGATE_SPOT"],
        defaultCapacityProviderStrategy: [
          { capacityProvider: "FARGATE", base: 0, weight: 1 },
          { capacityProvider: "FARGATE_SPOT", base: 0, weight: 1 },
        ],
      }
    );

    const coordinatorTask = new ecs.TaskDefinition(
      this,
      "CoordinatorTaskDefinition",
      {
        compatibility: ecs.Compatibility.EC2_AND_FARGATE,
        cpu: "256",
        memoryMiB: "512",
      }
    );
    coordinatorTask.addContainer("ci-coordinator", {
      cpu: 256,
      memoryLimitMiB: 512,
      memoryReservationMiB: 512,
      image: ecs.ContainerImage.fromAsset(
        path.resolve(__dirname, "images/ci-coordinator")
      ),
      linuxParameters: new ecs.LinuxParameters(
        this,
        "CoordinatorLinuxParameter",
        {
          initProcessEnabled: true,
        }
      ),
      environment: { LOG_FORMAT: "text" },
      logging: ecs.LogDrivers.awsLogs({
        streamPrefix: "GitLabRunnerCoordinator",
      }),
    });

    const coordinatorSecurityGroup = new ec2.SecurityGroup(
      this,
      "CoordinatorSecurityGroup",
      { vpc }
    );
    const gitlabRunnerTask = new ecs.TaskDefinition(
      this,
      "RunnerTaskDefinition",
      {
        compatibility: ecs.Compatibility.EC2_AND_FARGATE,
        cpu: "256",
        memoryMiB: "512",
      }
    );
    gitlabRunnerTask.addContainer("ci-runner", {
      cpu: 256,
      memoryLimitMiB: 512,
      memoryReservationMiB: 512,
      image: ecs.ContainerImage.fromAsset(
        path.resolve(__dirname, "images/ci-runner"),
        { file: "Dockerfile.pubnw" }
      ),
      environment: {
        CI_SERVER_URL: gitlabURL,
        REGISTRATION_TOKEN: gitlabRunnerToken,

        LOG_FORMAT: "json",
        RUNNER_NAME: this.stackName,
        RUNNER_TAG_LIST: Fn.join(",", [
          this.region,
          this.stackName,
          cluster.clusterName,
          coordinatorTask.family,
        ]),

        AWS_REGION: this.region,
        AWS_ECS_CLUSTER: cluster.clusterName,
        AWS_ECS_TASK_DEFINITION: coordinatorTask.family,
        AWS_VPC_SECURITYGROUP_ID: coordinatorSecurityGroup.securityGroupId,
        AWS_VPC_ENABLE_PUBLIC_IP: "true",
        AWS_VPC_SUBNET: vpc.publicSubnets[0].subnetId,
      },
      logging: ecs.LogDrivers.awsLogs({ streamPrefix: "GitLabRunner" }),
    });
    gitlabRunnerTask.taskRole.addManagedPolicy(
      iam.ManagedPolicy.fromAwsManagedPolicyName("AmazonECS_FullAccess")
    );
    const gitlabRunnerService = new ecs.FargateService(this, "RunnerService", {
      cluster: cluster,
      taskDefinition: gitlabRunnerTask,
      assignPublicIp: true,
      vpcSubnets: { subnets: vpc.publicSubnets },
    });
    coordinatorSecurityGroup.connections.allowFrom(
      gitlabRunnerService.connections,
      ec2.Port.tcp(22)
    );
  }
}
