#!/usr/bin/env bash

set -ueo pipefail

USER_SSH_KEYS_FOLDER=~/.ssh
if [[ ! -d "${USER_SSH_KEYS_FOLDER}" ]]; then
  mkdir -p "${USER_SSH_KEYS_FOLDER}"
fi

echo -ne "${SSH_PUBLIC_KEY}" >"${USER_SSH_KEYS_FOLDER}/authorized_keys"
unset SSH_PUBLIC_KEY

exec "$@"
