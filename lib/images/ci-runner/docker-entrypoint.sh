#!/usr/bin/env bash

set -ueo pipefail

export AWS_REGION=${AWS_REGION:-${AWS_DEFAULT_REGION:-""}}

sed -i \
  -e "s%{{ AWS_REGION }}%${AWS_REGION}%g" \
  -e "s%{{ AWS_ECS_CLUSTER }}%${AWS_ECS_CLUSTER}%g" \
  -e "s%{{ AWS_ECS_TASK_DEFINITION }}%${AWS_ECS_TASK_DEFINITION}%g" \
  -e "s%{{ AWS_VPC_ENABLE_PUBLIC_IP }}%${AWS_VPC_ENABLE_PUBLIC_IP}%g" \
  -e "s%{{ AWS_VPC_SECURITYGROUP_ID }}%${AWS_VPC_SECURITYGROUP_ID}%g" \
  -e "s%{{ AWS_VPC_SUBNET }}%${AWS_VPC_SUBNET}%g" \
  /etc/gitlab-runner/fargate.toml

/usr/local/bin/gitlab-runner register \
  --executor custom \
  --builds-dir "/opt/gitlab-runner/builds" \
  --cache-dir "/opt/gitlab-runner/cache" \
  --custom-run-exec /opt/gitlab-runner/fargate \
  --custom-run-args "--config" \
  --custom-run-args "/etc/gitlab-runner/fargate.toml" \
  --custom-run-args "custom" \
  --custom-run-args "run" \
  --custom-config-exec /opt/gitlab-runner/fargate \
  --custom-config-args "--config" \
  --custom-config-args "/etc/gitlab-runner/fargate.toml" \
  --custom-config-args "custom" \
  --custom-config-args "config" \
  --custom-prepare-exec /opt/gitlab-runner/fargate \
  --custom-prepare-args "--config" \
  --custom-prepare-args "/etc/gitlab-runner/fargate.toml" \
  --custom-prepare-args "custom" \
  --custom-prepare-args "prepare" \
  --custom-cleanup-exec /opt/gitlab-runner/fargate \
  --custom-cleanup-args "--config" \
  --custom-cleanup-args "/etc/gitlab-runner/fargate.toml" \
  --custom-cleanup-args "custom" \
  --custom-cleanup-args "cleanup" \
  --non-interactive

exec "$@"
