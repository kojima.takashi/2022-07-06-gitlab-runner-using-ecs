.PHONEY: deploy destroy

deploy:
	npx cdk deploy --parameters GitLabRunnerToken=$${GITLAB_RUNNER_TOKEN}

destroy:
	npx cdk destroy
